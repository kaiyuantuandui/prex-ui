import { axios } from '@/utils/request'

/*
 * 菜单管理模块
 */

// 保存
export const addMenu = (data) => {
  return axios({
    url: '/sys/menu',
    method: 'post',
    data: data
  })
}
// 删除
export const deleteMenu = (id) => {
  return axios({
    url: '/sys/menu/' + id,
    method: 'delete'
  })
}
// 查找导航菜单树
export const getMenuTree = () => {
  return axios({
    url: '/sys/menu',
    method: 'get'
  })
}

// 获取路由
export const getRouters = () => {
  return axios({
    url: '/sys/menu/getRouters',
    method: 'get'
  })
}

// 更新菜单
export function updateMenu(data) {
  return axios({
    url: '/sys/menu',
    method: 'put',
    data: data
  })
}

// 查找导航菜单树
export const queryMenuTree = () => {
  return axios({
    url: '/sys/menu/getMenuTree',
    method: 'get'
  })
}
